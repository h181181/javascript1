var fib = []; //array for the fibonacci numbers

fib[0] = 0; //first fibonacci number
fib[1] = 1; //second fibonacci number

//method for finding fibonacci numbers
for (var i = 2; i < 50; i++) {
	fib[i] = fib[i - 1] + fib[i - 2];
}

console.log(fib); //prints out the current fibonacci numbers
