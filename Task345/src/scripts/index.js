import "../styles/index.scss";
import * as moment from "moment";
import "./RPS";
import "./CalculatorScript";

//variable time with the current date and time
var currentTimeDate = moment().format("MMMM Do YYYY, h:mm:ss a");

// function to display the currentTime
function display() {
	const div = document.querySelector("#current-time");
	div.innerHTML = "<p>" + currentTimeDate + "</p>";
}
//call the method
display();
